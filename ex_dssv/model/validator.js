

var validator = {
    kiemTraSo : function (value,errorId,message){
        if(Number.isInteger(value)){
            document.getElementById(errorId).innerText = "";
            return true;
        } else {
            document.getElementById(errorId).innerText = message;
        }
    },
    kiemTraRong : function(value,errorId,message){
        if(value.length == 0){
            document.getElementById(errorId).innerText = `${message} không được để trống`;
            return false;
        } else {
            document.getElementById(errorId).innerText = "";
            return true;
        }
    },
    kiemTraDoDai : function(val,idError,message,minLen,maxLen){
        if(val<minLen||val>maxLen){
            document.getElementById(idError).innerText = `${message}`;
            return false;
        } else {
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    kiemTraEmail : function(dssv,input,idError,message){
        if(dssv.some(function(item){
            if(item.email == input){
                return true;
            }
        })){
            document.getElementById(idError).innerText = message;
            return false;
        } else {
            document.getElementById(idError).innerText = "";
            return true;
        }
        
    },
    kiemTraDinhDangEmail : function(value,idError,message){
        const regex =  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(regex.test(value)){
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    kiemTraId : function(dssv,inputId,idError,massage){
       if(dssv.some(
        function(item){
            if (item.ma == inputId){
                return true;
            }
        }
       )){
        console.log('test');
        document.getElementById(idError).innerText = massage;
        return false;
       } else {
        document.getElementById(idError).innerText = "";
        return true;
       }
    },
    kiemTraDiem : function(value,errorId,message){
        if(value.length == 0){
            document.getElementById(errorId).innerText = `${message} không được để trống` ;
            return false;
        } else if (isNaN(value)){
            document.getElementById(errorId).innerText = `${message} phải là một số thực` ;
            return false;
        } else {
            document.getElementById(errorId).innerText = `` ;
            return true;
        }
    }
}