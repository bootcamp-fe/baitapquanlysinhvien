
//  chức năng thêm sinh viên
var dssv = [];

// lấy thông tin từ localStorage

var dssvJason = localStorage.getItem("DSSV");
if (dssvJason!=null){
  var dssv = JSON.parse(dssvJason);
  for (var index = 0 ; index < dssv.length ; index++){
    var sv = dssv[index];
    dssv[index] = new SinhVien(sv.ten,sv.ma,sv.matKhau,sv.email,sv.toan,sv.ly,sv.hoa);
  }
  renderDSSV(dssv);
} 

function checkInputValid(newSv){
  var isValid = validator.kiemTraRong(newSv.ma,"spanMaSV","Mã SV") && validator.kiemTraDoDai(newSv.ma.length,"spanMaSV","Mã SV phải gồm 4 ký tự",4,4) && validator.kiemTraId(dssv,newSv.ma,"spanMaSV","Mã SV đã tồn tại") ;
  isValid = isValid &
  validator.kiemTraRong(newSv.ten,"spanTenSV","Tên") &
  validator.kiemTraRong(newSv.matKhau,"spanMatKhau","Mật khẩu")&
  validator.kiemTraDiem(newSv.toan,"spanToan","Điểm toán")&
  validator.kiemTraDiem(newSv.ly,"spanLy","Điểm lý")&
  validator.kiemTraDiem(newSv.hoa,"spanHoa","Điểm hóa");

  
  var isEmailValid =  validator.kiemTraRong(newSv.email,"spanEmailSV","Email") && validator.kiemTraDinhDangEmail(newSv.email,"spanEmailSV","Email không hợp lệ") && validator.kiemTraEmail(dssv,newSv.email,"spanEmailSV","Email đã được đăng ký")

  isValid = isValid & isEmailValid;

  return isValid;
}

function checkEditInputValid(newSv){
  var isValid = validator.kiemTraRong(newSv.ma,"spanMaSV","Mã SV") && validator.kiemTraDoDai(newSv.ma.length,"spanMaSV","Mã SV phải gồm 4 ký tự",4,4);
  isValid = isValid &
  validator.kiemTraRong(newSv.ten,"spanTenSV","Tên") &
  validator.kiemTraRong(newSv.matKhau,"spanMatKhau","Mật khẩu")&
  validator.kiemTraDiem(newSv.toan,"spanToan","Điểm toán")&
  validator.kiemTraDiem(newSv.ly,"spanLy","Điểm lý")&
  validator.kiemTraDiem(newSv.hoa,"spanHoa","Điểm hóa");

  
  var isEmailValid =  validator.kiemTraRong(newSv.email,"spanEmailSV","Email") && validator.kiemTraDinhDangEmail(newSv.email,"spanEmailSV","Email không hợp lệ");

  isValid = isValid & isEmailValid;

  return isValid;
}

function themSV() {
  var newSv = layThongTinTuForm();
  var isValid = checkInputValid(newSv);
  if(isValid){
    dssv.push(newSv);
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dssvJson);
    renderDSSV(dssv);
  }
  
}

function capNhatThongTinSV(){
  var idCapNhat = document.getElementById('txtMaSV').value;
  var index = findIndex(idCapNhat,dssv);
  if(index!=-1){
    var thongTinMoiSV = layThongTinTuForm();
    var isInputValid = checkEditInputValid(thongTinMoiSV);
    if(isInputValid){
      dssv.splice(index,1,thongTinMoiSV);
      dssvJason = JSON.stringify(dssv);
      localStorage.setItem("DSSV",dssvJason);
      renderDSSV(dssv);
      tatTrangThaiChinhSua();
      resetInputVal();
    }
  }
}

function resetInputVal(){
  document.querySelectorAll('.col-6 .form-group > input').forEach((item) =>{
    item.value = "";
  })
  document.querySelectorAll('.col-6 .form-group > span').forEach(function(item){
    item.innerText = "";
  })
  document.getElementById("txtMaSV").disabled = false;
  document.getElementById("themSVBtn").disabled = false;
  document.getElementById("btnSearch").disabled = true;
  document.getElementById("updateDataBtn").disabled = true;
  renderDSSV(dssv);
}

// Enable search Btn

document.getElementById('txtSearch').addEventListener('input',(e)=>{
  if(e.target.value.length>1){
    document.getElementById('btnSearch').disabled = false;
  } else {
    document.getElementById('btnSearch').disabled = true;
  }
})

console.log(dssv);


// Search Btn

function searchName(){
  var searchValue = document.getElementById("txtSearch").value;
  var filteredDssv = dssv.filter(function(item){
    if(item.ten == searchValue){
      return true;
    } else {
      return false;
    }
  });
  if(filteredDssv.length != 0){
    console.log(filteredDssv)
    renderDSSV(filteredDssv);
  } else {
    alert(`Không tìm thấy ${searchValue}`);
  }
}
