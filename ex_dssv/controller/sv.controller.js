function layThongTinTuForm() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(tenSv, maSv, matKhau, email, diemToan, diemLy, diemHoa);
}

// render array sv ra giao diện
function renderDSSV(svArr) {
  // contentHTML : chuỗi chứa các thẻ <tr></tr>;
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    // trContent thẻ tr trong mỗi lần lặp
    var trContent = ` <tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button onclick = "suaThongTinSV('${sv.ma}')"  class="btn btn-info"><i class="fa-regular fa-pen-to-square"></i></button>
    <button onclick = "xoaSV('${sv.ma}')" class="btn btn-warning"><i class="fa-regular fa-trash-can"></i></button>
    </td>
    </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
  // tbodySinhVien;
}


function findIndex(id,dssv){
  for(var index = 0 ; index < dssv.length; index++){
    var sv = dssv[index];
    if(sv.ma == id){
      return index;
    }
  }
  return -1;
}

function xoaSV(id){
  var index = findIndex(id,dssv);
  if(index!=-1){
    dssv.splice(index,1);
    dssvJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dssvJson);
    renderDSSV(dssv);
  }
}

function hienThongTinChinhSua(id){
  var thongTinSV = dssv[id];
  document.getElementById("txtMaSV").value=thongTinSV.ma;
  document.getElementById("txtTenSV").value=thongTinSV.ten;
  document.getElementById("txtEmail").value=thongTinSV.email;
  document.getElementById("txtPass").value=thongTinSV.matKhau;
  document.getElementById("txtDiemToan").value=thongTinSV.toan;
  document.getElementById("txtDiemLy").value=thongTinSV.ly;
  document.getElementById("txtDiemHoa").value=thongTinSV.hoa;
}

function batTrangThaiChinhSua(){
  var inputMaSV = document.getElementById("txtMaSV");
  var themSVBtn = document.getElementById("themSVBtn");
  var chinhSuaBtn = document.getElementById("updateDataBtn");
  inputMaSV.disabled = true;
  themSVBtn.disabled = true;
  chinhSuaBtn.disabled = false;
}

function tatTrangThaiChinhSua(){
  // var inputMaSV = document.getElementById("txtMaSV");
  // var themSVBtn = document.getElementById("themSVBtn");
  var chinhSuaBtn = document.getElementById("updateDataBtn");
  // inputMaSV.disabled = true;
  // themSVBtn.disabled = true;
  chinhSuaBtn.disabled = true;
}

function suaThongTinSV(id){
  var index = findIndex(id,dssv);
  hienThongTinChinhSua(index);
  batTrangThaiChinhSua();
}